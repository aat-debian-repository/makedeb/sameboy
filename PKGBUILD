# Maintainer: Antoni Aloy <aaloytorrens@gmail.com>

# From Arch Linux AUR:
# Contributor: Arvedui <arvedui@posteo.de>
# Contributor: Jakub Kądziołka <kuba@kadziolka.net>

pkgname=sameboy
pkgdesc="An accuracy-focused Game Boy/Game Boy Color emulator"
pkgver=0.16.6
pkgrel=2
arch=("amd64")
url="https://github.com/LIJI32/SameBoy"
license=("MIT")
depends=("libsdl2-2.0-0" "hicolor-icon-theme")
# Upstream suggests using clang, but gcc is supported on Linux: https://github.com/LIJI32/SameBoy/issues/164#issuecomment-486464194
makedepends=("rgbds" "make" "libsdl2-dev")
source=(
	"sameboy-shell"
	"${pkgname}-${pkgver}.tar.gz::https://github.com/LIJI32/SameBoy/archive/v${pkgver}.tar.gz"
	)

sha256sums=('e176fbf23658230804f57c07feb80f4466b9eec015ba1a960004bf2761dda8d8'
            '025ffb47582afd9098093db6ab95435dab2cd6006824601f8eb2b522433aa00a')

build(){
	cd "${srcdir}/SameBoy-${pkgver}"
	make sdl CONF=release DATA_DIR=/usr/share/games/sameboy CC=gcc FREEDESKTOP=true
}

package(){
	cd "${srcdir}/SameBoy-${pkgver}"

	make install CONF=release PREFIX=/usr DATA_DIR=/usr/share/games/sameboy DESTDIR="${pkgdir}" FREEDESKTOP=true
	install -D "${pkgdir}/usr/share/games/sameboy/LICENSE" "${pkgdir}/usr/share/licenses/sameboy/LICENSE"

	# seems that sameboy will not work unless we put the executable in the DATA_DIR
	mv "${pkgdir}"/usr/bin/sameboy "${pkgdir}"/usr/share/games/sameboy

	# Install sameboy-shell executable
	install -Dm755 $srcdir/sameboy-shell "${pkgdir}"/usr/games/sameboy

	# mimetype icons don't belong here
	# that could lead to file conflicts
	find "${pkgdir}" -name 'x-gameboy*-rom.png' -delete
	find "${pkgdir}" -name mimetypes -delete

}
